/*
 * ----------------------------------------------------------------------------------
 * Notes:
 *
 * 1) Varchar vs text (not supported by all databases): https://www.depesz.com/2010/03/02/charx-vs-varcharx-vs-varchar-vs-text/
 *
 * ----------------------------------------------------------------------------------
*/

CREATE TABLE addresses (
	id  serial NOT NULL, 
	address varchar(255) NOT NULL, 
	city varchar(255) NOT NULL, 
	PRIMARY KEY (id)
);

CREATE TABLE users (
	id  serial NOT NULL, 
	email varchar(255) NOT NULL UNIQUE, 
	name varchar(255) NOT NULL UNIQUE, 
	password varchar(255) NOT NULL,
	address_id int4, 
	PRIMARY KEY (id),
	FOREIGN KEY (address_id) REFERENCES addresses (id)
);

CREATE TABLE roles (
	id  serial NOT NULL, 
	authority varchar(255) NOT NULL, 
	user_id int4 NOT NULL, 
	PRIMARY KEY (id),
	UNIQUE (id, user_id),
	FOREIGN KEY (user_id) REFERENCES users (id)
);


