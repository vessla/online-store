-- username: paulina, password: adminPassword, authorities: ADMIN
INSERT INTO users(name, email, password) VALUES ('paulina', 'pjadamska@gmail.com', '$2a$11$msf5G44vNDfmFw289nZCtuuGakY7MdRs1PgDgJ2OKZuh7CnZDsUuS');
-- username: julia, password: customerPassword
INSERT INTO users(name, email, password) VALUES ('julia', 'jjadamska@gmail.com', '$2a$11$rwyyj/p9EGA6kWfNWCRiR.noj95Mt5mOEHznrtr754b8l8Gpr4eN.');
 
INSERT INTO roles (authority, user_id) VALUES ('ADMIN', '1');
INSERT INTO roles (authority, user_id) VALUES ('CUSTOMER', '2');