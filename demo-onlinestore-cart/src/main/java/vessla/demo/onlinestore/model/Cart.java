package vessla.demo.onlinestore.model;

import java.math.BigInteger;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.Id;

import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "cart")
public class Cart {

	// Integer type is not accepted as id for MongoDB (use String or BigInteger) 
	// (see: https://docs.spring.io/spring-data/mongodb/docs/current/reference/html/#mongo-template.id-handling)
	@Id 
    private BigInteger id;
	
	//Expiration-associated field must be a date type or an array of date types (see: https://docs.mongodb.com/manual/core/index-ttl/)
	@Indexed(name = "expireAfterSecondsIndex", expireAfterSeconds = 3600)
	private LocalDateTime timestamp;
	
	private Integer customerId;
	
	private Integer quantity;
	
	//By default MongoDB stores the items directly in the 'cart' collection
	private Map<Integer, CartItem> items;

	public Cart(Integer customerId){
		this.customerId = customerId;
		quantity = 0;
		timestamp = LocalDateTime.now();
		items = new HashMap<>();
	}
	
	public int addProduct(Integer productId){
		CartItem previouslyAddedItem = items.get(productId);
		if(previouslyAddedItem != null){
			previouslyAddedItem.setQuantity(previouslyAddedItem.getQuantity()+1);
		}
		else{
			items.put(productId, new CartItem(productId, 1));
		}
		setQuantity(getQuantity()+1);
		
		return getQuantity();
	}
	
	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public Integer getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	public Map<Integer, CartItem> getItems() {
		return items;
	}

	public void setItems(Map<Integer, CartItem> items) {
		this.items = items;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	
}
