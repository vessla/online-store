package vessla.demo.onlinestore.model;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "cartitem")
public class CartItem {
	
	private Integer productId;
	
	private Integer quantity;
	
	public CartItem(int productId, int quantity){
		this.productId = productId;
		this.quantity = quantity;
	}
	
	public int getProductId() {
		return productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
	        return true;
		else if (o == null)
	        return false;
		else{
			CartItem compareTo = (CartItem)o;
			return this.getProductId()==compareTo.getProductId()?true:false;
		}
	}
}
