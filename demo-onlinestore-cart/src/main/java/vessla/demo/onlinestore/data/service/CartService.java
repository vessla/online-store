package vessla.demo.onlinestore.data.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import vessla.demo.onlinestore.data.dao.CartRepository;
import vessla.demo.onlinestore.model.Cart;

@Service
public class CartService {

	@Autowired
	private CartRepository cartRepository;
	
	public Cart getCartByCustomerId(int customerId){
		return cartRepository.findOneByCustomerId(customerId);
	}
	
	public void saveCart(Cart c){
		cartRepository.save(c);
	}
	
}
