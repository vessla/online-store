package vessla.demo.onlinestore.data.dao;

import org.springframework.data.mongodb.repository.MongoRepository;

import vessla.demo.onlinestore.model.Cart;

public interface CartRepository extends MongoRepository<Cart, Long>{
	
	Cart findOneByCustomerId(Integer id);
	
}
