package vessla.demo.onlinestore.data.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import vessla.demo.onlinestore.data.dao.UserRepository;
import vessla.demo.onlinestore.model.User;

//Service created for Spring Security (see usage in: vessla.demo.onlinestore.OnlineStoreSecurityConfig)

@Service
public class UserDetailsServiceImpl implements UserDetailsService{

	@Autowired
	private UserRepository userRepository;
	
	public UserDetails loadUserByUsername(String name) throws UsernameNotFoundException{ 
		User user = userRepository.findOneByUsername(name);
		if(user == null){ 
			throw new UsernameNotFoundException("Invalid username or password!"); 
		} 
		return user;
	} 
}
