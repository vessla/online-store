package vessla.demo.onlinestore.config;

import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@EnableMongoRepositories(basePackages = "vessla.demo.onlinestore.data.dao")
public class OnlineStoreMongoConfiguration {

}
