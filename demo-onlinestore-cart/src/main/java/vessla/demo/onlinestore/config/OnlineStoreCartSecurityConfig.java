package vessla.demo.onlinestore.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import vessla.demo.onlinestore.data.service.UserDetailsServiceImpl;
import vessla.demo.onlinestore.model.User;

@Configuration
@EnableWebSecurity
public class OnlineStoreCartSecurityConfig extends WebSecurityConfigurerAdapter {
	
	@Autowired
	private UserDetailsServiceImpl userDetailsService;
	
	public boolean checkCustomerId(Authentication authentication, int id) {
		return ((User)authentication.getPrincipal()).getId().equals(id);
	}
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests().antMatchers("/cart/{customerId}/**").access("@onlineStoreCartSecurityConfig.checkCustomerId(authentication,#customerId)").
		anyRequest().authenticated().
		and().httpBasic().
		and().csrf().disable();;
		
	}
	
	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
	}
	
	@Bean 
	public PasswordEncoder passwordEncoder(){ 
		PasswordEncoder encoder = new BCryptPasswordEncoder(11); 
		return encoder; 
	}
	
}
