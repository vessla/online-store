package vessla.demo.onlinestore;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoOnlinestoreCartApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoOnlinestoreCartApplication.class, args);
	}
}
