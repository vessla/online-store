package vessla.demo.onlinestore.web.controller;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import vessla.demo.onlinestore.data.service.CartService;
import vessla.demo.onlinestore.model.Cart;

@RestController
@RequestMapping("/cart/{customerId}")
public class CartController {
	
	private static Logger logger = Logger.getLogger(CartController.class.getName());
	
	@Autowired
	private CartService cartService;

	@PostMapping("/newItem")
    public ResponseEntity<Integer> addItem(HttpSession session, @PathVariable Integer customerId, @RequestBody Integer productId) {
		logger.debug("Processing /newItem request started");
		
		Cart customerCart = cartService.getCartByCustomerId(customerId);
		if(customerCart == null)
			customerCart = new Cart(customerId);
		customerCart.addProduct(productId);
		
		cartService.saveCart(customerCart);
		session.setAttribute("cart_itemCount", customerCart.getQuantity());
		
		return new ResponseEntity<Integer>(customerCart.getQuantity(), HttpStatus.OK);
    }
	
	//TODO: Extend the implementation, when the shopping cart gui is ready for it ;) )
	@GetMapping("/getItems")
	ResponseEntity<Integer> getItems(HttpSession session, @PathVariable Integer customerId) {
		Cart customerCart = cartService.getCartByCustomerId(customerId);
		if(customerCart == null)
			customerCart = new Cart(customerId);
		
		cartService.saveCart(customerCart);
		session.setAttribute("cart_itemCount", customerCart.getQuantity());
		
		return new ResponseEntity<Integer>(customerCart.getQuantity(), HttpStatus.OK);
	}
	
}
