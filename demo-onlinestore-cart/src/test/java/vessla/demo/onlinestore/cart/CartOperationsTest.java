package vessla.demo.onlinestore.cart;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.collection.IsMapContaining.hasKey;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import vessla.demo.onlinestore.model.Cart;

@RunWith(SpringRunner.class)
public class CartOperationsTest {
	
	@Test
	public void testAddNonExistingProductToCart() {
		Cart cart = new Cart(123);
		cart.addProduct(2);
		
		assertThat(cart.getItems().size(), equalTo(1));
	    assertThat(cart.getItems(), hasKey(2));
	    assertThat(cart.getItems().get(2), hasProperty("productId", equalTo(2)));
	    assertThat(cart.getItems().get(2), hasProperty("quantity", equalTo(1)));
	    assertThat(cart, hasProperty("quantity", equalTo(1)));
	}
	
	@Test
	public void testAddExistingProductToCart() {
		Cart cart = new Cart(123);
		cart.addProduct(2);
		cart.addProduct(2);
		
		assertThat(cart.getItems().size(), equalTo(1));
	    assertThat(cart.getItems(), hasKey(2));
	    assertThat(cart.getItems().get(2), hasProperty("productId", equalTo(2)));
	    assertThat(cart.getItems().get(2), hasProperty("quantity", equalTo(2)));
	    assertThat(cart, hasProperty("quantity", equalTo(2)));
	}
	
	@Test
	public void testAddExistingProductsToCart() {
		Cart cart = new Cart(123);
		cart.addProduct(2);
		cart.addProduct(2);
		cart.addProduct(2);
		
		assertThat(cart.getItems().size(), equalTo(1));
	    assertThat(cart.getItems(), hasKey(2));
	    assertThat(cart.getItems().get(2), hasProperty("productId", equalTo(2)));
	    assertThat(cart.getItems().get(2), hasProperty("quantity", equalTo(3)));
	    assertThat(cart, hasProperty("quantity", equalTo(3)));
	}

}
