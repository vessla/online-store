## Setup

```
Package both the 'onlinestore' and the 'onlinestore-cart' jars and copy them to the 'onlinestore' and 'shoppingcart' directories respectively (using maven).

WARNING: By default nginx (loadbalancer/proxy.conf) is configured to use 192.168.0.4 as the "public" host IP visible for the client for all the services (one may want to change this before building docker images).

docker-compose build
```

## Start

```
docker-compose up -d --scale onlinestore=3
```

Open a browser on `https://192.168.0.4` and accept the self-signed certificate

