package vessla.demo.onlinestore.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.security.core.GrantedAuthority;

@Entity
@Table(name="roles", uniqueConstraints = {@UniqueConstraint(columnNames = {"id", "user_id"})})
public class Role implements GrantedAuthority{

	private static final long serialVersionUID = 11L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY) 
	@Column(name="id")
	private Integer id;
	
	@NotBlank 
	@Length(min = 1, max = 255)
	@Column(name="authority", nullable=false)
	private String authority;
	
	@NotNull
	@ManyToOne(fetch = FetchType.LAZY) 
	@JoinColumn(name="user_id")
	private User user;

	//------------------------------------------------
	// Constructors
	//------------------------------------------------
		
	public Role(){
		this("", new User());
	}
	
	public Role(String authority){
		this(authority, null);
	}
		
	public Role(String authority, User user){
		this.authority = authority;
		this.user = user;
	}
		
	//---------------------------------------------------------------------
	// Getters & setters
	//---------------------------------------------------------------------	
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAuthority() {
		return authority;
	}

	public void setAuthority(String authority) {
		this.authority = authority;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	//---------------------------------------------------------------------
	// Other 
	//---------------------------------------------------------------------
	
	public String toString(){
		return getAuthority();
	}
}
