package vessla.demo.onlinestore.model;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

@Entity
@Table(name="users")
public class User implements UserDetails{
	
	private static final long serialVersionUID = 11L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY) 
	@Column(name="id")
	private Integer id;
	
	@NotBlank 
	@Length(min = 1, max = 255)
	@Column(name="name", nullable=false, unique=true)
	private String username;
	
	@Email
	@Column(name="email", nullable=false, unique=true)
	private String email;
	
	@NotBlank
	@Column(name="password", nullable=false)
	private String password;

	@NotNull
	@OneToMany(fetch = FetchType.EAGER, mappedBy="user") 
	private Collection<Role> authorities;
	
	@OneToOne(fetch = FetchType.LAZY, cascade=CascadeType.ALL) 
	@JoinColumn(name="address_id")
	private Address address;
	
	//------------------------------------------------
	// Constructors
	//------------------------------------------------
	
	public User(){
		this("", "", "", new ArrayList<Role>());
	}
	
	public User(String name, String email, String password, Collection<Role> authorities){
		this.username = name;
		this.email = email;
		this.password = password;
		this.authorities = authorities;
	}
	
	//---------------------------------------------------------------------
	// Getters & setters
	//---------------------------------------------------------------------	
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String name) {
		this.username = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Collection<? extends GrantedAuthority> getAuthorities() {
		return authorities;
	}

	public void setAuthorities(Collection<Role> authorities) {
		this.authorities = authorities;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}
	
	//---------------------------------------------------------------------
	// Methods defined in UserDetails interface 
	// (these attributes have not yet been used in the demo model)  
	//---------------------------------------------------------------------	

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

}
