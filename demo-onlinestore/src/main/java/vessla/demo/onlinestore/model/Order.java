package vessla.demo.onlinestore.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="orders")
public class Order {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY) 
	@Column(name="id")
	private Integer id;
	
	@Column(name="order_date", nullable=false)
	private LocalDateTime date;
	
	//TODO: add links to the items from the cart
	
	//------------------------------------------------
	// Constructors
	//------------------------------------------------
	
	public Order(){
		this(LocalDateTime.now());
	}
	
	public Order(LocalDateTime date){
		this.date = date;
	}
	
	//---------------------------------------------------------------------
	// Getters & setters
	//---------------------------------------------------------------------
}
