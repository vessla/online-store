package vessla.demo.onlinestore.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotBlank;

@Entity
@Table(name="addresses")
public class Address {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY) 
	@Column(name="id")
	private Integer id;
	
	@NotBlank 
	@Column(name="city", nullable=false)
	private String city;
	
	@NotBlank 
	@Column(name="address", nullable=false)
	private String addressDetails;

	//------------------------------------------------
	// Constructors
	//------------------------------------------------
	
	public Address(){
		this("", "");
	}
	
	public Address(String city, String fullAddress){
		this.city = city;
		this.addressDetails = fullAddress;
	}
	
	//---------------------------------------------------------------------
	// Getters & setters
	//---------------------------------------------------------------------	
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getFullAddress() {
		return addressDetails;
	}

	public void setFullAddress(String fullAddress) {
		this.addressDetails = fullAddress;
	}
	
	
}
