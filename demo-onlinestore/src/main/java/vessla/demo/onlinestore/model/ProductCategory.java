package vessla.demo.onlinestore.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

@Entity
@Table(name="categories")
public class ProductCategory {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY) 
	@Column(name="id")
	private Integer id;
	
	@NotBlank 
	@Length(min = 1, max = 255)
	@Column(name="name", nullable=false, unique=true)
	private String name;
	
	//------------------------------------------------
	// Constructors
	//------------------------------------------------
	
	public ProductCategory(){
		this("");
	}
	
	public ProductCategory(String name){
		this.name = name;
	}
	
	//------------------------------------------------
	// Getters & setters
	//------------------------------------------------
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
