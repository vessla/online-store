package vessla.demo.onlinestore.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.web.multipart.MultipartFile;

@Entity
@Table(name="products", uniqueConstraints = {@UniqueConstraint(columnNames = {"name", "category_id"})})
public class Product {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY) 
	@Column(name="id")
	private Integer id;
	
	@NotBlank 
	@Length(min = 1, max = 255)
	@Column(name="name", nullable=false, unique=true)
	private String name;
	
	@NotNull
	@OneToOne(fetch = FetchType.EAGER) 
	@JoinColumn(name="category_id")
	private ProductCategory category; 
	
	@NotBlank
	@Column(name="description", nullable=false)
	private String description;
	
	@Column(name="price", nullable=false)
	private Double price;
	
	@Column(name="quantity", nullable=false)
	private Integer quantity;
	
	@Column(name="image_url", nullable=false)
	private String imageUrl;
	
	@Transient
	private MultipartFile image;
	
	//------------------------------------------------
	// Constructors
	//------------------------------------------------
	
	public Product(){
		this("", new ProductCategory(), "", 0.0, 0, "");
	}
	
	public Product(String name, ProductCategory category, String description, double price, int itemsInStock, String imageUrl){
		this.name = name;
		this.category = category;
		this.description = description;
		this.price = price;
		this.quantity = itemsInStock;
		this.imageUrl = imageUrl;
	}
	
	//---------------------------------------------------------------------
	// Getters & setters
	//---------------------------------------------------------------------
	
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public double getPrice() {
		return price;
	}
	
	public void setPrice(double price) {
		this.price = price;
	}
	
	public int getItemsInStock() {
		return quantity;
	}
	
	public void setItemsInStock(int itemsInStock) {
		this.quantity = itemsInStock;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public MultipartFile getImage() {
		return image;
	}

	public void setImage(MultipartFile image) {
		this.image = image;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public ProductCategory getCategory() {
		return category;
	}

	public void setCategory(ProductCategory category) {
		this.category = category;
	}
	
}
