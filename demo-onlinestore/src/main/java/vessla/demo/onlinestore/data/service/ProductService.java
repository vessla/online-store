package vessla.demo.onlinestore.data.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import vessla.demo.onlinestore.data.dao.ProductRepository;
import vessla.demo.onlinestore.model.Product;

@Service
public class ProductService {

	@Autowired
	private ProductRepository productRepository;
	
	public List<Product> getRecentlyAdded(int count){
		return productRepository.findAllByOrderByIdDesc(new PageRequest(0, 3));
	}
	
	public Page<Product> getAll(Pageable page){
		return productRepository.findAllByOrderByNameAsc(page);
	}
	
	public Page<Product> getAllByCategoryName(String categoryName, Pageable page){
		return productRepository.findByCategoryNameOrderByName(categoryName, page);
	}
}
