package vessla.demo.onlinestore.data.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import vessla.demo.onlinestore.data.dao.ProductCategoryRepository;
import vessla.demo.onlinestore.model.ProductCategory;

@Service
public class ProductCategoryService {
	
	@Autowired
	private ProductCategoryRepository categoryRepository;
	
	public Iterable<ProductCategory> getCategories(){
		return categoryRepository.findAll(new Sort(Direction.ASC, "name"));
	}
}
