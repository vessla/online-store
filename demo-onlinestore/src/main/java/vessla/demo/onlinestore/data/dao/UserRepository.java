package vessla.demo.onlinestore.data.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import vessla.demo.onlinestore.model.User;

public interface UserRepository extends JpaRepository<User, Long>{
	
	User findOneByUsername(String name);
	
}
