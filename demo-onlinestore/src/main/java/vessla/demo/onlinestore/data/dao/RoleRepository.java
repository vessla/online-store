package vessla.demo.onlinestore.data.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import vessla.demo.onlinestore.model.Role;

public interface RoleRepository extends JpaRepository<Role, Byte>{
	
}
