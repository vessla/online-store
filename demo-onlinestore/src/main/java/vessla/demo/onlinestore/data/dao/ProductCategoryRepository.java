package vessla.demo.onlinestore.data.dao;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;

import vessla.demo.onlinestore.model.ProductCategory;

/*
 * Documentation for automatically implemented methods:
 * https://docs.spring.io/spring-data/data-commons/docs/1.6.1.RELEASE/reference/html/repositories.html
 * 
 * "CRUD methods on repository instances are transactional by default" (see http://docs.spring.io/spring-data/data-jpa/docs/1.0.3.RELEASE/reference/html/#transactions)
 */

public interface ProductCategoryRepository extends PagingAndSortingRepository<ProductCategory, Integer>{
	
	List<ProductCategory> findByName(String name);
}
