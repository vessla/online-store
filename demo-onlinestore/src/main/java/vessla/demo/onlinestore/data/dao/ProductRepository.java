package vessla.demo.onlinestore.data.dao;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import vessla.demo.onlinestore.model.Product;

public interface ProductRepository extends PagingAndSortingRepository<Product, Integer>{
	
	Page<Product> findByCategoryNameOrderByName(String categoryName, Pageable pageable);
	
	Page<Product> findAllByOrderByNameAsc(Pageable pageable);
	
	List<Product> findAllByOrderByIdDesc(Pageable pageable);
	
}
