package vessla.demo.onlinestore.config;

import java.util.Locale;

import org.springframework.boot.context.embedded.EmbeddedServletContainerCustomizer;
import org.springframework.boot.web.servlet.ErrorPage;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

@Configuration
public class OnlineStoreWebConfig extends WebMvcConfigurerAdapter {
	
	@Override
	public void addViewControllers(ViewControllerRegistry registry){
		/* 
		 * Redirecting all the errors to homepage just to keep the demo 
		 * project as simple as possible
		 * 
		 */
		registry.addViewController("/notFound").setViewName("redirect:/");
		registry.addViewController("/badRequest").setViewName("redirect:/");
	}
	
    @Bean
    public EmbeddedServletContainerCustomizer containerCustomizer() {
        return container -> {
            container.addErrorPages(new ErrorPage(HttpStatus.NOT_FOUND, "/notFound"));
            container.addErrorPages(new ErrorPage(HttpStatus.BAD_REQUEST, "/badRequest"));
        };
    }
	
	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		//Internationalization
	    registry.addInterceptor(localeChangeInterceptor());
	}
	
	//----------------------------------------------------
	// Beans required for Thymeleaf internationalization
	//----------------------------------------------------
	
	@Bean
	public LocaleResolver localeResolver() {
	    SessionLocaleResolver slr = new SessionLocaleResolver();
	    slr.setDefaultLocale(Locale.US);
	    return slr;
	}
	
	//Automatic support for additional ?lang=XX URL parameter
	@Bean
	public LocaleChangeInterceptor localeChangeInterceptor() {
	    LocaleChangeInterceptor lci = new LocaleChangeInterceptor();
	    lci.setParamName("lang");
	    return lci;
	}
	
	//Adding support for messages.properties files
	@Bean
	public MessageSource messageSource() {
		ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
		messageSource.setBasename("locale/messages");
		messageSource.setDefaultEncoding("UTF-8");
		return messageSource;
	}
	
	//----------------------------------------------------

}
