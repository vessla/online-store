package vessla.demo.onlinestore.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import vessla.demo.onlinestore.data.service.ProductCategoryService;
import vessla.demo.onlinestore.data.service.ProductService;
import vessla.demo.onlinestore.model.Product;

@Controller
@RequestMapping("/")
public class AnonymousController {
	
	@Autowired
	private ProductService productService;
	
	@Autowired
	private ProductCategoryService categoryService;
	

	@GetMapping("/") 
	public String viewHomePage(Model model){
		model.addAttribute("recentlyAddedProducts", productService.getRecentlyAdded(3));
		//Attribute required by the navbar dropdown menu "Products"
		model.addAttribute("categories", categoryService.getCategories());
		return "layout"; 
	}

	/*
	 * Separate method for AJAX request instead of detecting AJAX using the X-Requested-With
	 * header (apparently the header is not required to be set by the client libraries)
	 */
	@GetMapping("/home") 
	public String viewHomePageAjax(Model model){
		model.addAttribute("recentlyAddedProducts", productService.getRecentlyAdded(3));
		model.addAttribute("categories", categoryService.getCategories());
		return "fragments/marketing :: contents";
	}
	
	@GetMapping("/navbar") 
	public String viewNavbarAjax(Model model){
		model.addAttribute("recentlyAddedProducts", productService.getRecentlyAdded(3));
		model.addAttribute("categories", categoryService.getCategories());
		return "fragments/navbar :: navbar";
	}
	
	@GetMapping("/product/all") 
	public String viewProductList(@RequestParam(value="cat", required=true, defaultValue="all") String categoryName, Pageable page, Model model){
		Page<Product> foundProducts;

		if(!categoryName.equals("all")){
			foundProducts = productService.getAllByCategoryName(categoryName, page);
		}else{
			foundProducts = productService.getAll(page);	
		}
		model.addAttribute("products", foundProducts);
		model.addAttribute("hasNext", foundProducts.hasNext());
		model.addAttribute("hasPrevious", foundProducts.hasPrevious());
		model.addAttribute("hasContent", foundProducts.hasContent());
		model.addAttribute("currentPage", foundProducts.getNumber());
		model.addAttribute("currentCategory", categoryName);
		
		return "fragments/productlist :: contents";
	}
	
	@GetMapping("/login") 
	public String login() {
	    return "fragments/login :: contents";
	} 
	
}
