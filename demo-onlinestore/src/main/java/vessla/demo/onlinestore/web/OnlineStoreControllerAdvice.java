package vessla.demo.onlinestore.web;

import org.springframework.web.bind.annotation.ControllerAdvice;

@ControllerAdvice
public class OnlineStoreControllerAdvice {
	
	/*
	 * The method below are no longer required, due to moving to AJAX 
	 * (the navbar is only reloaded and filled with categories, when the whole home 
	 * page is reloaded, so moved this part to the 'viewHomePage' controller method)
	 * 
	 *
	 *
	 * @ModelAttribute
     * public void globalAttributes(Model model) {
	 *		//Attribute required by the navbar dropdown menu "Products"
	 *		model.addAttribute("categories", categoryService.getCategories());
     * }
     * 
     * /
	
	/*
	 * Placeholder for handling exceptions thrown by all the controllers 
	 * WARNING: this is not suitable for handling the servlet container-generated errors!
	 * 			(see the OnlineStoreWebConfig methods for this)
	 * 
	 * @ExceptionHandler(Exception.class)
     * public ModelAndView handleError(HttpServletRequest request, Exception e)   {
     *  	Logger.getLogger(getClass().getName()).log(Level.ERROR, "Request: " + request.getRequestURL() + " raised " + e);
     *  	return new ModelAndView("name");
     * }
     * 
	 */
} 
