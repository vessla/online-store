INSERT INTO categories (name) VALUES ('mp3');
INSERT INTO categories (name) VALUES ('cd');

INSERT INTO products (name, category_id, description, price, quantity, image_url) VALUES ('Les Revenants', 1, 'Soundtrack by Mogwai', 20.99, 1000, 'http://res.cloudinary.com/dgz8tqqeo/image/upload/v1502908383/les-revenants-p-iext46097114_ixn59m.jpg');
INSERT INTO products (name, category_id, description, price, quantity, image_url) VALUES ('Les Revenants', 2, 'Soundtrack by Mogwai', 50.99, 10, 'http://res.cloudinary.com/dgz8tqqeo/image/upload/v1502908383/les-revenants-p-iext46097114_ixn59m.jpg');
INSERT INTO products (name, category_id, description, price, quantity, image_url) VALUES ('Despicable Me 3', 2, 'Original Motion Picture Soundtrack', 39.99, 100, 'http://res.cloudinary.com/dgz8tqqeo/image/upload/v1502908386/gru-dru-i-minionki-despicable-me-3-p-iext49963273_dshhip.jpg');
INSERT INTO products (name, category_id, description, price, quantity, image_url) VALUES ('Meteora', 2, 'Album by Linkin Park, 2003', 42.49, 50, 'http://res.cloudinary.com/dgz8tqqeo/image/upload/v1504340847/meteora-p-iext41773895_mlluto.jpg');
INSERT INTO products (name, category_id, description, price, quantity, image_url) VALUES ('S & M', 2, 'Metallica''s ninth album, with Michael Kamen conducting the San Francisco Symphony Orchestra', 69.99, 15, 'http://res.cloudinary.com/dgz8tqqeo/image/upload/v1504340788/s-m-p-iext43231419_vprrh9.jpg');
INSERT INTO products (name, category_id, description, price, quantity, image_url) VALUES ('Living Things', 2, 'The fifth studio album by American rock band Linkin Park, 2012', 29.99, 35, 'http://res.cloudinary.com/dgz8tqqeo/image/upload/v1504340722/living-things-p-iext40504372_vzjeqd.jpg');
INSERT INTO products (name, category_id, description, price, quantity, image_url) VALUES ('Lindsey Stirling (Deluxe Edition)', 2, 'Deluxe version of her first album!', 40.99, 35, 'http://res.cloudinary.com/dgz8tqqeo/image/upload/v1504340657/lindsey-stirling-deluxe-edition-p-iext46050729_vy1cyk.jpg');
INSERT INTO products (name, category_id, description, price, quantity, image_url) VALUES ('Home', 2, 'Original Motion Picture Soundtrack', 46.99, 12, 'http://res.cloudinary.com/dgz8tqqeo/image/upload/v1504340545/home-dom-p-iext37748279_f250mk.jpg');

-- username: paulina, password: adminPassword, authorities: ADMIN
INSERT INTO users(name, email, password) VALUES ('paulina', 'pjadamska@gmail.com', '$2a$11$msf5G44vNDfmFw289nZCtuuGakY7MdRs1PgDgJ2OKZuh7CnZDsUuS');
-- username: julia, password: customerPassword
INSERT INTO users(name, email, password) VALUES ('julia', 'jjadamska@gmail.com', '$2a$11$rwyyj/p9EGA6kWfNWCRiR.noj95Mt5mOEHznrtr754b8l8Gpr4eN.');
  
 
INSERT INTO roles (authority, user_id) VALUES ('ADMIN', '1');
INSERT INTO roles (authority, user_id) VALUES ('CUSTOMER', '2');