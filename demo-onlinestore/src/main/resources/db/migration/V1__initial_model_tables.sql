/*
 * ----------------------------------------------------------------------------------
 * Notes:
 *
 * 1) Varchar vs text (not supported by all databases): https://www.depesz.com/2010/03/02/charx-vs-varcharx-vs-varchar-vs-text/
 *
 * ----------------------------------------------------------------------------------
*/

CREATE TABLE categories (
	id  serial NOT NULL, 
	name varchar(255) NOT NULL UNIQUE, 
	PRIMARY KEY (id)
);

CREATE TABLE products (
	id  serial NOT NULL,
	description text NOT NULL, 
	image_url text NOT NULL, 
	name varchar(255) NOT NULL, 
	price float8 NOT NULL, 
	quantity int4 NOT NULL, 
	category_id int4 NOT NULL, 
	PRIMARY KEY (id),
	UNIQUE (name, category_id),
	FOREIGN KEY (category_id) REFERENCES categories (id)
);

CREATE TABLE addresses (
	id  serial NOT NULL, 
	address varchar(255) NOT NULL, 
	city varchar(255) NOT NULL, 
	PRIMARY KEY (id)
);

CREATE TABLE users (
	id  serial NOT NULL, 
	email varchar(255) NOT NULL UNIQUE, 
	name varchar(255) NOT NULL UNIQUE, 
	password varchar(255) NOT NULL,
	address_id int4, 
	PRIMARY KEY (id),
	FOREIGN KEY (address_id) REFERENCES addresses (id)
);

CREATE TABLE roles (
	id  serial NOT NULL, 
	authority varchar(255) NOT NULL, 
	user_id int4 NOT NULL, 
	PRIMARY KEY (id),
	UNIQUE (id, user_id),
	FOREIGN KEY (user_id) REFERENCES users (id)
);

CREATE TABLE orders (
	id  serial NOT NULL, 
	order_date bytea NOT NULL, 
	primary key (id)
)


