$( document ).ready(function() {
	
	$(document).click(function (event) {
		//hide the collapsed navbar when user clicks somewhere else
	    if($("#navbar").is(":visible") && $("#navbarToggle").is(":visible")){
	        $('#navbar').collapse('hide');
	    }	        
	});
	
	//'Home' button from the navbar
	$('#navbarcontainer').on('click', '#navbarnavhome', function(event){
		ajaxLoadContent(event, $(this));
	});
	
	//'Sign in" button from the navbar
	$('#navbarcontainer').on('click', '#loginbtn', function(event){
		ajaxLoadContent(event, $(this));
	});
	
	//'Categories' dropdown list from the navbar
	$('#navbarcontainer').on('click', 'a.productlistnavbarnav', function(event){
		ajaxLoadContent(event, $(this));
	});
	
	//'Next page' and 'Previous page' in the product list
	$('#contents').on('click', 'a.productlistpagenav', function(event){
		ajaxLoadContent(event, $(this));
	});
	
});

function ajaxLoadContent(event, caller){
	$("#pleaseWaitDialog").modal();
	event.preventDefault();
	$('#contents').load(caller.attr('href'), function(){
		$("#pleaseWaitDialog").modal('hide');
	});
}