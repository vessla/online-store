$( document ).ready(function() {
	
	var url = window.location;
	
	$('#contents').on('click', '.btn-addtocart', function(event){
		event.preventDefault();
		ajaxAddToCart($(this).data("product"), $("#customer").text());
	});
	
	function ajaxAddToCart(productId, customerId){	
		$.ajax({
			type : "POST",
			contentType: "application/json",
			data: JSON.stringify(productId),
	        dataType: 'json',
			url : url+"cart/"+customerId+"/newItem",
			success: function(result){
				console.log(result);
				$("#cartItemCount").text(result);
			},
			error : function(e) {
				console.log("ERROR: ", e);
			}
		});	
	}
	
})