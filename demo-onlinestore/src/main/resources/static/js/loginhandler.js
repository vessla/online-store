$( document ).ready(function() {
	
	var url = window.location;
	
	/* To make the declaration below work, the tags shown below where added to header.html:
	 * <meta th:name="_csrf" th:content="${_csrf.token}"/>
	 * <meta th:name="_csrf_header" th:content="${_csrf.headerName}"/>
	 */
	var token = $("meta[name='_csrf']").attr("content");
	
	$('#contents').on('submit', '#loginform', function(event){
		$('#loadingContent').show();
		event.preventDefault();
		
		$.ajax({
			type : "POST",
			contentType: "application/x-www-form-urlencoded",
			data: $(this).serialize(),
			url : url+"login",
			beforeSend: function(xhr) { //due to csrf validation on the server side
		        xhr.setRequestHeader('X-CSRF-TOKEN', token);
		    },
			success: function(result){
				$("#contents").html(result);
				if($("#loggedinUser").text()){
					//reloading the navbar for authenticated user
					$('#navbarcontainer').load(url+"navbar", function(){
						//getting shopping cart item number if not expired
						var customerId = $("#customer").text();
						if(customerId.length > 0){
							console.log("Trying to send request to get the shopping cart items")
							ajaxGetCart(url, customerId);
						}
					});
				}
				$('#loadingContent').hide();
			},
			error : function(e) {
				$('#loadingContent').hide();
				console.log(e);
			}
		});	
	});

});

function ajaxGetCart(url, customerId){
	$.ajax({
		type : "GET",
		url : url+"cart/"+customerId+"/getItems",
		success: function(cartItems){
			$("#cartItemCount").text(cartItems);
		},
		error : function(e) {
			console.log("ERROR: ", e);
		}
	});
}