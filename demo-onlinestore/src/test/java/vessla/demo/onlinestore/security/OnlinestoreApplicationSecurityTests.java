package vessla.demo.onlinestore.security;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.authenticated;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@TestPropertySource(locations="classpath:application-test.properties")
public class OnlinestoreApplicationSecurityTests {
	
	@Autowired
	private MockMvc mockMvc;
	
	/*
	 * Alternative to the " authorities="ADMIN" " is " roles="ADMIN" ", however it 
	 * requires ROLE_ prefix in the database (eg. ROLE_ADMIN) 
	 */
	@Retention(RetentionPolicy.RUNTIME)
	@WithMockUser(value="paulina", authorities="ADMIN") //roles="ADMIN" this one requires ROLE_ prefix
	public @interface WithMockAdmin { }
	
	@Retention(RetentionPolicy.RUNTIME)
	@WithMockUser(value="julia", authorities="CUSTOMER")
	public @interface WithMockCustomer { }
	
	@Retention(RetentionPolicy.RUNTIME)
	@WithMockUser(value="joanna", authorities={"ADMIN", "CUSTOMER"})
	public @interface WithMockAdminCustomer { }
	
	@Test
	@WithMockAdmin //This annotation can also be applied to the entire test class
	//@WithUserDetails(value="paulina", userDetailsServiceBeanName="userDetailsServiceImpl")
	public void requestAdminProtectedUrlWithAdminUserReturns200() throws Exception {
		Collection<GrantedAuthority> expectedAuthorities = new ArrayList<GrantedAuthority>();
		expectedAuthorities.add(new SimpleGrantedAuthority("ADMIN"));
		
		mockMvc.perform(get("/admin/product/new"))
			   .andExpect(authenticated().withUsername("paulina"))
			   .andExpect(authenticated().withAuthorities(expectedAuthorities))
			   .andExpect(status().isOk());
	}
	
	@Test
	@WithMockAdminCustomer
	public void requestAdminProtectedUrlWithAdminCustomerUserReturns200() throws Exception {
		Collection<GrantedAuthority> expectedAuthorities = new ArrayList<GrantedAuthority>();
		expectedAuthorities.add(new SimpleGrantedAuthority("ADMIN"));
		expectedAuthorities.add(new SimpleGrantedAuthority("CUSTOMER"));
		
		mockMvc.perform(get("/admin/product/new"))
			   .andExpect(authenticated().withUsername("joanna"))
			   .andExpect(authenticated().withAuthorities(expectedAuthorities))
			   .andExpect(status().isOk());
	}
	
	@Test
	@WithMockCustomer
	public void requestAdminProtectedUrlWithCustomerUserReturns403() throws Exception {
		Collection<GrantedAuthority> expectedAuthorities = new ArrayList<GrantedAuthority>();
		expectedAuthorities.add(new SimpleGrantedAuthority("CUSTOMER"));
		
		mockMvc.perform(get("/admin/product/new"))
			   .andExpect(authenticated().withUsername("julia"))
			   .andExpect(authenticated().withAuthorities(expectedAuthorities))
			   .andExpect(status().isForbidden()); 
	}
	
	@Test
	@WithAnonymousUser
	public void requestAdminProtectedUrlWithAnonymousUserRedirectsToLogin() throws Exception {
		MvcResult mvcResult = this.mockMvc.perform(get("/admin/product/new"))
				.andExpect(status().is3xxRedirection())
				.andReturn();

		assertThat(mvcResult.getResponse().getRedirectedUrl()).endsWith("/login");
	}
	
}
