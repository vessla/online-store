package vessla.demo.onlinestore.repositories;

import static org.hamcrest.collection.IsIterableContainingInOrder.contains;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasProperty;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import vessla.demo.onlinestore.data.dao.ProductCategoryRepository;
import vessla.demo.onlinestore.model.ProductCategory;

/* TIPS:
 * For tests of the JPA layer with a separate (in-memory) database 
 * (assuming maven dependency for the supported embedded database in pom.xml)
 * use the annotations below: 
 * @RunWith(SpringRunner.class)
 * @DataJpaTest
 * 
 * Not using this feature now, because of different results of constraint violation test 
 * in comparison to the PostgreSQL database that will be used in production (other differences 
 * possible). Using separate PostgreSQL configurations instead. 
 *
 * For Java classes with test/production-specific configuration use profiles:
 * https://docs.spring.io/spring-boot/docs/current/reference/html/boot-features-profiles.html
 *
 */

@RunWith(SpringRunner.class)
@SpringBootTest()
@TestPropertySource(locations="classpath:application-test.properties")
public class ProductCategoryRepositoryTest {

	@Autowired
	ProductCategoryRepository productCategoryRepository;
	
	@Before
    public void prepareDatabaseContents() throws Exception {
    	ProductCategory category = new ProductCategory("cd");
    	productCategoryRepository.save(category);
    	
    	category = new ProductCategory("audiobook");
    	productCategoryRepository.save(category);
    }
	
	@Test
	public void testFindAllSortByIdAsc() {
	     List<ProductCategory> categories = (ArrayList<ProductCategory>) productCategoryRepository.findAll();
	     
	     assertThat(categories.size(), equalTo(2));
	     assertThat(categories, contains(hasProperty("name", equalTo("cd")), hasProperty("name", equalTo("audiobook"))));
	}

	@Test
	public void testFindAllSortByNameAsc() {
	     List<ProductCategory> categories = (ArrayList<ProductCategory>) productCategoryRepository.findAll(new Sort(Direction.ASC, "name"));
	     
	     assertThat(categories.size(), equalTo(2));
	     assertThat(categories, contains(hasProperty("name", equalTo("audiobook")), hasProperty("name", equalTo("cd"))));
	}
	
	@Test
	public void testFindOneByName() {
		List<ProductCategory> categories = productCategoryRepository.findByName("audiobook");
		
		assertThat(categories.size(), equalTo(1));
	    assertThat(categories.get(0), hasProperty("name", equalTo("audiobook")));
	}
	
	@Test(expected = org.springframework.dao.DataIntegrityViolationException.class) 
	public void testSaveViolatingUniqueNameConstraint() {
		ProductCategory category = new ProductCategory("cd");
		productCategoryRepository.save(category);
	}

	@After
	public void clearDatabase(){
		productCategoryRepository.deleteAll();
	}
	
}
