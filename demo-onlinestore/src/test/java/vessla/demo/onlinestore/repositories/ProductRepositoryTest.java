package vessla.demo.onlinestore.repositories;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsIterableContainingInOrder.contains;
import static org.hamcrest.Matchers.hasProperty;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import vessla.demo.onlinestore.data.dao.ProductCategoryRepository;
import vessla.demo.onlinestore.data.dao.ProductRepository;
import vessla.demo.onlinestore.model.Product;
import vessla.demo.onlinestore.model.ProductCategory;

/* TIPS:
 * For tests of the JPA layer with a separate (in-memory) database 
 * (assuming maven dependency for the supported embedded database in pom.xml)
 * use the annotations below: 
 * @RunWith(SpringRunner.class)
 * @DataJpaTest
 * 
 * Not using this feature now, because of different results of constraint violation test 
 * in comparison to the PostgreSQL database that will be used "in production" (other differences possible). 
 */

@RunWith(SpringRunner.class)
@SpringBootTest(/*webEnvironment=WebEnvironment.RANDOM_PORT*/)
@TestPropertySource(locations="classpath:application-test.properties")
public class ProductRepositoryTest {

	@Autowired
	ProductRepository productRepository;
	
	@Autowired
	ProductCategoryRepository productCategoryRepository;
	
	@Before
    public void prepareDatabaseContents() throws Exception {
		ProductCategory category = new ProductCategory("cd");
		productCategoryRepository.save(category);
		Product product = new Product("Les Revenants", category, "CD by Mogwai", 40.99, 5, "#");
		productRepository.save(product);
		product = new Product("Gru, Dru and Minions", category, "Soundtrack", 50.99, 10, "#");
		productRepository.save(product);

		category = new ProductCategory("ebook");
		productCategoryRepository.save(category);
		product = new Product("Vattnet drar", category, "Book by Madeleine Back", 30.59, 2, "#");
		productRepository.save(product);
    }
	
	@Test
	public void testFindAllByCategoryNamePageSizeLarger() {
		Page<Product> products = (Page<Product>) productRepository.findByCategoryNameOrderByName("cd", new PageRequest(0, 3));
	    assertThat(products.getNumberOfElements(), equalTo(2));
	    assertThat(products, contains(hasProperty("name", equalTo("Gru, Dru and Minions")), hasProperty("name", equalTo("Les Revenants"))));
	}
	
	@Test
	public void testFindAllByCategoryNamePageSizeSmaller() {
		Page<Product> products = (Page<Product>) productRepository.findByCategoryNameOrderByName("cd", new PageRequest(0, 1));
	    assertThat(products.getNumberOfElements(), equalTo(1));
	    assertThat(products, contains(hasProperty("name", equalTo("Gru, Dru and Minions"))));
	}

	@After
	public void clearDatabase(){
		productRepository.deleteAll();
		productCategoryRepository.deleteAll();
	}
	
}
