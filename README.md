# README #

### What is this repository for? ###

The main goal of this project was to document the skills regarding different technologies and concepts in practice, not to implement a production-ready e-commerce website. 
The project consists of two main components:

1. The [main application](https://bitbucket.org/vessla/demoonlinestore/src/5075c30c788e6a8dadef685cac8d5b6042f10760/demo-onlinestore/?at=master) responsible for generating the views and searching for products
2. [Shopping cart](https://bitbucket.org/vessla/demoonlinestore/src/5075c30c788e6a8dadef685cac8d5b6042f10760/demo-onlinestore-cart/?at=master) REST service (MongoDB is configured to store the shopping cart items for one hour)

The architecture of the entire application is neither pure monolithic nor pure microservices due to the fact, that the goal of this project was to present practical usage of a fairly large amount of mechanisms, while keeping the application (and its Docker Compose configuration) relatively simple. Nevertheless both the main application and the shopping cart service can be scaled up to multiple instances.

The main application supports localization with respect to gui-related messages (product descriptions are only available in English). All the images associated with the products are stored in the [Cloudinary](https://cloudinary.com/) service.  

### How do I run it? ###

See the Docker Compose configuration in the [README.MD](https://bitbucket.org/vessla/demoonlinestore/src/b060dff59373/demo-onlinestore-docker/?at=master) file.

External libraries/tools used in the project:

1. Frontend: [Bootstrap](https://getbootstrap.com/), jQuery (Ajax), Thymeleaf 
2. Database password encryption: [Jasypt](https://github.com/ulisesbocchio/jasypt-spring-boot) 
3. Data storage: [Flyway](https://flywaydb.org/), Hibernate, MongoDB, PostgreSQL
4. Session externalization: [Jedis](https://github.com/xetorthio/jedis)
5. Testing: [JUnit](https://junit.org/junit4/), [Hamcrest](http://hamcrest.org/JavaHamcrest/)

## Screenshots

### Application on PC
![Home page (logged in as customer)](demo-onlinestore-screens/komp_screen_1.jpg "Home page, logged in as customer")

![Product details (logged in as customer)](demo-onlinestore-screens/komp_screen_3.jpg "Product details, logged in as customer")

![Home page (logged in as admin)](demo-onlinestore-screens/komp_screen_5.jpg "Home page, logged in as admin")

### Application on mobile phone
![Mobile phone view](demo-onlinestore-screens/tel_screenshot_2017-10-13_123830.jpg)

### Application on tablet
![Tablet view](demo-onlinestore-screens/tab_screenshot_2017-10-13_123601.jpg)

### Who do I talk to? ###

pjadamska@gmail.com